def assignment(new_list, i_new, old_list, j_old):
    """
	Function to assign the value of old_list at position j_old to the position i_new in new_list
    -------------------------------------------------------------------
    
	new_list: List we want to change a value [type: list]
	i_new: Position of new_list, where we want the new value [type: int]
	old_list: List from which we get the new value [type: list]
	j_new: Position of old list, where the desired value is [type: int]
    """

    new_list[i_new] = old_list[j_old]


def merge_sort(input_list):
    """
	Function to sort the list by the magnitude of its values (smallest values at the beginning of the list)
    -------------------------------------------------------------------
    
	input_list: List which we want to sort [type: list]
    """

    if (len(input_list) > 1):
        mid_point = len(input_list) // 2 # Middle of the input list
	
    	# Split the input list in the middle:
        left_list = input_list[:mid_point]
        right_list = input_list[mid_point:]

        merge_sort(left_list)
        merge_sort(right_list)

        left_counter = 0
        right_counter = 0
        iterator_input_list = 0

        # Check if we are not at the end of left and right list:
        while left_counter < len(left_list) and right_counter < len(right_list):
            
            # Check if value of left list is smaller than value of right list:
            if left_list[left_counter] <= right_list[right_counter]:
                assignment(new_list=input_list, i_new=iterator_input_list, old_list=left_list, j_old=left_counter)
                left_counter += 1
            
            else:
                assignment(new_list=input_list, i_new=iterator_input_list, old_list=right_list, j_old=right_counter)
                right_counter += 1

            iterator_input_list += 1

        # Check if we are not at the end of left list:
        while left_counter < len(left_list):
            assignment(new_list=input_list, i_new=iterator_input_list, old_list=left_list, j_old=left_counter)
            left_counter += 1
            iterator_input_list += 1

        # Check if we are not at the end of right list:
        while right_counter < len(right_list):
            assignment(new_list=input_list, i_new=iterator_input_list, old_list=right_list, j_old=right_counter)
            right_counter += 1
            iterator_input_list += 1



import matplotlib.pyplot as plt


plt.figure(figsize=(7, 5))

my_list = [54, 26, 93, 17, 77, 31, 44, 55, 20]
x = range(len(my_list))
plt.scatter(x, my_list, marker = ".", label = "Original data", s=90)
plt.plot(x, my_list, alpha=0.3, linestyle = '--')

merge_sort(my_list)
x = range(len(my_list))
plt.scatter(x, my_list, marker = ".", color = "darkorange", label = "Sorted data", s=90)
plt.plot(x, my_list, color = "darkorange", alpha=0.3, linestyle = '--')

plt.xlabel("List position", size = "15") 
plt.ylabel("Magnitude of value", size = "15")
plt.ylim((10, 110))
plt.tick_params(axis = "both", labelsize = 12)

plt.legend(loc = "upper left",  frameon = False)
plt.show()
